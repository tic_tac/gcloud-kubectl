FROM alpine:3.7
MAINTAINER Jules AGOSTINI

ARG GCLOUD_VERSION=249.0.0

ARG KUBECTL_VERSION=v1.14.0

# install dependencies
RUN apk add --no-cache ca-certificates tar wget openssl python bash 
RUN mkdir /opt

# install gcloud
RUN	wget -q https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz \
    && tar -xvf google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz \
    && mv google-cloud-sdk /opt/google-cloud-sdk \
    && /opt/google-cloud-sdk/install.sh \
    && rm -f google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz

# install kubectl
RUN wget -q https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl -O /opt/google-cloud-sdk/bin/kubectl \
    && chmod a+x /opt/google-cloud-sdk/bin/kubectl

ENV PATH=$PATH:/opt/google-cloud-sdk/bin

CMD ["bash"]